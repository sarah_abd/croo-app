import React from 'react'

import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import { green } from '@material-ui/core/colors';
import Link from '@material-ui/core/Link';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import Typography from '@material-ui/core/Typography';
import { createMuiTheme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TableComments from './tableComments'
import imgComment from '../../img/tel.jpg'
import {
  createComment, getComments,
} from "../../services/commentsServices";

import { socket } from '../../Pages/homePage'

function Copyright() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://gitlab.com/sarah_abd/croo-app" style={{ fontWeight:'bold' }}>
          Croo technical challenge
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }
  const theme = createMuiTheme({
  palette: {
    primary: green,
  },
  test:{
    color:"red"
  }
});

  class MyForm extends React.Component{

      constructor(props) {
        super(props);

        this.state = {
          username : '',
          email: '',
          comment:'',
          completeComment:{},
          itemArray:this.props.comments ? this.props.comments:[]
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
      }
  
    
      handleSubmit = async (event) => {

        const item = this.state.itemArray;
        let createdComment = undefined;
        let comment =
        {
          username:this.state.username,
          email:this.state.email,
          comment:this.state.comment,
          date:new Date().toJSON().slice(0,10).replace(/-/g,'/'),
        }
        item.push(comment);
        this.setState({itemArray: item})
        this.props.saveData(this.state.itemArray)

        event.preventDefault();
        //createdComment = await createComment(comment); an axios command
        
        const response = await fetch("/createComment", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(comment)


        // }).then(res => 
        //     console.log(socket.emit('CommentAdded', {'data' : {'comment': comment}}))
        //   )

        // }).then(res => {
        //   socket.emit('CommentAdded', {'data' : {'comment': comment}})
        })


        // .then(data=>console.log(data));
        // console.log(comment);
        //console.log( res)
        if(response){
          if (response.ok) {
            window.location.reload(false);
            console.log("response worked!");
          }
        }
      }

      handleInputChange(event) {
        
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
          [name]: value
        });
      }

    render() {
    return (
      <Container component="main" > 
        <CssBaseline />
        <div style= {{
             marginTop: theme.spacing(8),
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
          }}>
      <img src={imgComment}  style={{ width: '75%',height:'25vw',marginBottom: theme.spacing(3)}} alt="company"/>

            <TableComments rows = {this.state.itemArray} comments={this.props.comments}/>
          <Avatar style= {{
              margin: theme.spacing(1),
              backgroundColor: theme.palette.primary.main,
          }} >
            <ChatBubbleIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Leave a comment
          </Typography>
          <form style={{width: '75%',marginTop: theme.spacing(1) }} onSubmit={this.handleSubmit}>
          <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="your name"
              name="username"
              value={this.state.username}
              onChange={this.handleInputChange} 
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              value={this.state.email}
              onChange={this.handleInputChange} 
            />
            <TextField
            id="comment"
            label="Enter your comment ..."
            name="comment"
            multiline
            fullWidth
            rows={4}
            variant="outlined"
            required
            onChange={this.handleInputChange} 
            value={this.state.comment}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              style={{ color: "white", backgroundColor:"green", margin: theme.spacing(3, 0, 2) }} 
            >
              Submit comment
            </Button>
      </form>
        </div>
        <Box mt={8}>
          <Copyright />
        </Box>
      </Container>
    );}}
  
export const MyComment =(props)=>{
    return ( 
    <div>
      <MyForm saveData ={props.saveData} comments={props.comments} ></MyForm>

      {/* <MyForm onSaveComments={props.onSaveComments}></MyForm> */}
    </div>
  )
  
}