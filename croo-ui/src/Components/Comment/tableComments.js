import React from 'react';
import { makeStyles,withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.success.dark
      ,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  
  const useStyles = makeStyles({
    table: {
      width:"75%", 
    },
    tableHead: {
        fontWeight:"bold", 
      },
  });
  
  export default function CustomizedTables(props) {
    const classes = useStyles();
    //const rows = props.comments.data.comments; 
    //console.log(props.comments.data.comments)
    
  
    return (
      props.comments.data ?
        props.comments.data.comments.length === 0  ? <h2 style={{color:"black"}}>No comments yet</h2> : ( 
        <TableContainer component={Paper} className={classes.table} >
        <Table  aria-label="customized table">
          <TableHead> 
            <TableRow>
              <StyledTableCell  >Date </StyledTableCell>
              <StyledTableCell align="left">Comments</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.comments.data.comments.map((row,key) => (
              
              <StyledTableRow key={key}>
                <StyledTableCell component="th" scope="row">
            {row.date} <p >By <a href={"mailto:"+row.email} style={{color:"purple"}}>{row.username}</a> </p>
                </StyledTableCell>
            <StyledTableCell align="left">{row.comment}</StyledTableCell>
             
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>):
      <div>data undefined</div>
    );
  }