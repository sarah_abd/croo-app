import React, {useState, useEffect}from 'react' 
import {MyComment} from "../Components/Comment/comment"
import io from 'socket.io-client';
//const io = require("socket.io-client")
export const socket = io('http://localhost:5000/');



export const Homepage = (props)=> {

    const [myData, setMyData] = useState([])
    const [myIoComments, setIoComments] = useState([])
    const ioComments = myData;
    useEffect (()=>{

      socket.on('commentAddedResponse',  (resp) => {
        
        const commentAdded = resp.data.data.comment;
        console.log(commentAdded)
        //ioComments.push(commentAdded)
        console.log(ioComments)
      })
      // }).then(data => console.log(data)) ;
     // }).then(data => setIoComments({data})) ;

     
    },[])

    
    useEffect (()=>{
        fetch('/comments').then(response=>{
            if(response.ok){
                return response.json()
            }
         // }).then(data => console.log(data.comments)) //data.comments is an array of database comments 
         }).then(data => setMyData({data})) // on avait console.log avantt
    },[])

    return (
      
    <MyComment  comments = {myData} saveData ={props.saveData}></MyComment>
      //<MyComment   data={myData}   comments = {props.comments} saveData ={props.saveData}  ></MyComment>

    )
}

 