

/*
 * Copyright (c) 2021.
 *  Project Name:
 *       croo technical challenge app.
 * 
 * Project developper:
 *    Sarah Abdelaoui <Sarah.Abdelaoui@hotmail.com>
 *
 * Prototype not finished 
 * Details : 
 * - axios already installed, axios services implemented, but not used ...
 * - socket Io not finished 
 * - ...
 */


import './App.css';
import React from 'react'
import {Homepage} from './Pages/homePage'
// import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";




class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      comments: [],
      comment: {},
      theComments:[]
    };
    this.saveData = this.saveData.bind(this);
  }
  
  saveData = (data) => { 
    console.log(data)
    this.setState({theComments:data})
  }


  render() {

  return (
    <div className="App">
        <div>
          <Homepage saveData ={this.saveData} comments={this.state.theComments}></Homepage>
        </div>

    </div>
  );
  }
}

export default App;
