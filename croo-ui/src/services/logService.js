import Raven from "raven-js";

function init() {
  Raven.config(
    "http://7a8877359a5540fda4d866cf4e7a5efd@sentry.aerowill.net/13",
    {
      release: "0.0.1-SNAPSHOT",
      environment: "development"
    }
  ).install();
}

function log(error) {
  Raven.captureException(error);
}

export default {
  init,
  log
};
