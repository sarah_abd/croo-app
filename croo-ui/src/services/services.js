import http from "./httpServices";

export async function get(endpoint) {
  const { data } = await http.get(endpoint);
  return data;
}

export async function post(endpoint, data) {
  console.log( http.post(endpoint, data));
  const { data: object } = await http.post(endpoint, data);
  
  return object;
}

export const remove = async (endpoint, id) => {
  return http.delete(endpoint + "/" + id);
};


