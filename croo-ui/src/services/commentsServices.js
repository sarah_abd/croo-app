import { get, post } from "./services";
import { commentsApi } from "../config.json";


export function getComments() {
  const comments = get(commentsApi);
  return comments;
}


export function createComment(comment) {
   console.log(comment);
  const newComment = post(commentsApi, comment);
  return newComment;
}


