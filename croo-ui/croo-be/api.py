from flask import Flask,render_template, url_for, request, jsonify
from flask_mysqldb import MySQL
from flask_socketio import *

app = Flask(__name__)


app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'sarah1427'
app.config['MYSQL_DB'] = 'crooApp'

mysql = MySQL(app)


# SocketIO 
socketio = SocketIO(app, cors_allowed_origins="*")

def init_db():
    db = get_db()
    with app.open_resource('croo-db.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

@app.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    init_db()
    print('Initialized the database.')

@app.route('/api',methods=['GET'])
def index():
    return {
        #request to the database to get the array of comments les soqyer dans le dom au lieu de les afficher avec console.log
        'name' : ['should', 'return','data','from','db']
    }

@app.route('/comments',methods=['GET'])
def displayComments():
    mysql_select_query = """SELECT * from crooApp.comments"""
    db_create_query = """CREATE DATABASE IF NOT EXISTS crooApp;
                         CREATE TABLE IF NOT EXISTS crooApp.comments
                         (id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, myUsername VARCHAR(255), myEmail VARCHAR(255),  myComment VARCHAR(255), myDate VARCHAR (100))
                      """
    cur = mysql.connection.cursor()
    cur.execute(db_create_query)
    cur.execute(mysql_select_query)
    comments = cur.fetchall()
    #build dict here

    #print(comments)
    newComments=[]
    for comment in comments:
        # print(comment[1])
        comment={'username':comment[1],'email':comment[2],'comment':comment[3],'date':comment[4]}
        newComments.append(comment)
    print(comments)
    print(newComments)
    mysql.connection.commit()
    cur.close()

    return {'comments' : newComments}

@app.route('/createComment',methods=['POST'])
def createComment():
    data = request.get_json()
    print((data))
    cur = mysql.connection.cursor()
    # mysql_insert_query =  
    cur.execute("INSERT INTO comments(myUsername, myEmail, myComment, myDate) VALUES (%s, %s,%s, %s)", (data['username'], data['email'], data['comment'],data['date']))
    mysql.connection.commit()
    cur.close()
    return 'success'
    #return render_template('../index.html')


# SocketIO Events
@socketio.on('connect')
def connected():
    print('Connected')

@socketio.on('disconnect')
def disconnected():
    print('Disconnected')

@socketio.on('CommentAdded')
def commentAdded(message):
    print('Comment Added')
    emit('commentAddedResponse', {'data': message}, broadcast=True)
    print(message)


# if __name__ == '__main__':
#     app.run(debug=True)

if __name__ == '__main__':
    socketio.run(app, debug=True)


