# Croo app

# Project: Croo technical test  

Build a full app, client side, server and database  

## Description

We received the mandate to design a web application *Display user comments*, an app where users can write comments and should be persisted in a database, and displayed in real time for all users. 


## author

- Sarah Abdelaoui

## Supported platforms

This application was created and tested under the **macOS** platform


## Commands during developpement:

```
     source ./venv/bin/activate
```
 to activate the virtual environnement from the croo-app/croo-ui/croo-be folder 

 connect react application to the back-end: 
 create proxy on package.json file of react application, proxy to the flask url 


 Create a .env file 
 Add the library that will use it with:

```
 pip install python-dotenv
```

 https://itnext.io/start-using-env-for-your-flask-project-and-stop-using-environment-variables-for-development-247dc12468be

 commands for db: CREATE DATABASE IF NOT EXISTS comments; 

pip3 install mysql-connector-python 

## Run project 

1- From croo-ui/croo-be : python3 api.py 
2- From croo-ui : yarn start 

## Installation

The microframework used is Flask for Python based on Jinja2, it's used to simplify the use of HTTP

To run the application you must first install Flask

The installation of Flask is done using the command:

```
     pip3 install Flask
```


* The environment variable is defined in **the makefile**, this must be done before launching the application and is defined as follows:

```
    export FLASK_APP=index.py
```


## Features requested and implemented

... to be done

...

## Database

...

Deman features A database is located at the root of the project containing the `SQL` script

A creation of the database is necessary before using the application, in particular its manipulation and this is done
as shown in the following procedure:

Download and install MySQL Community Server and MySQL Workbench. You can skip this step if you already have a MySQL server installed.


```
    brew install mysql
```

Creating a python dictionnary (python file) wich contains the credentials to connect to the MySQL server database

## Technologies used

 **front-end**: ReactJs

 **back-end**: Flask, MySQL Workbench, socketIo



## Project content

croo-ui front end folder
croo-db back end folder

README.md file 
.....



## Références

https://flask.palletsprojects.com/en/0.12.x/
https://fr.reactjs.org/
https://github.com/wwt/flask-SocketIO 



